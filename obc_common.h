/*
* Copyright (C) 2020 Akim Lesovets
* This file is part of bzzz-obc-open <https://gitlab.com/a.lesovets/bzzz-obc-open>.
*
* bzzz-obc-open is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* bzzz-obc-open is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with bzzz-obc-open.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * obc_common.h
 *
 *  Created on: 13 окт. 2020 г.
 *      Author: AKIM
 */

#ifndef OBC_COMMON_H_
#define OBC_COMMON_H_
#include "stdint.h"
void ComMetersCopy(float *dst,float *src, uint8_t *map, uint8_t len);

#endif /* OBC_COMMON_H_ */
