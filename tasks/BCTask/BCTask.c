/*
* Copyright (C) 2020 Akim Lesovets
* This file is part of bzzz-obc-open <https://gitlab.com/a.lesovets/bzzz-obc-open>.
*
* bzzz-obc-open is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* bzzz-obc-open is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with bzzz-obc-open.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * BCTask.c
 *
 *  Created on: 10 окт. 2020 г.
 *      Author: AKIM
 */
#include "BCTask.h"
#include "avr/interrupt.h"
#include <FreeRTOS.h>
#include <portmacro.h>
#include <projdefs.h>
#include <string.h>
#include "task.h"
#include "semphr.h"
#include "lstLib.h"
#include "obc_projdefs.h"
#include "maths.h"
#include "utils.h"
#include "avr/pgmspace.h"
#include "obc_common.h"

volatile uint32_t tacho[2]={0,};
extern float meters[];
extern float adc_meters[];
extern config_t config;
extern SemaphoreHandle_t xMeterSemaphoreHandle;
float bc_meters[BC_METER_MAX]={0.0,};
float bc_dist_delta=0.0;

uint8_t PROGMEM bc_to_meter_map[BC_METER_MAX]={
		[BC_METER_RPM1]=METER_RPM1,
		[BC_METER_RPM2]=METER_RPM2,
		[BC_METER_ODO1]=METER_ODO1,
		[BC_METER_ODO2]=METER_ODO2,
		[BC_METER_SPEED]=METER_SPEED,
		[BC_METER_ECO]=METER_ECO,
		[BC_METER_RANGE]=METER_RANGE,
};

void vApplicationTickHook( void )
{
	uint8_t pins=PIND;
	static uint8_t old_pins=0;
	static uint8_t first=0;
	if(first)
	{
		if((pins&HALL1)!=(old_pins&HALL1))
			tacho[0]++;
		if((pins&HALL2)!=(old_pins&HALL2))
			tacho[1]++;
	}
	else
		first=0xff;
	old_pins=pins;
}

#define BCTASK_RATE		(250)

uint32_t TachoToRPM(uint32_t delta,uint32_t last_update)
{
	return(delta*60000/config.magnets/last_update);
}

__attribute__ ((OS_main)) void vBCTask(void *param)
{
	portTickType xTickCur;
	portTickType xTickOld=0;
	portTickType xTickDelata;
	portTickType xLastWakeTime;
	uint32_t old_tacho[2]={0,};
	bc_meters[BC_METER_ODO2]=config.dist;
	xLastWakeTime = xTaskGetTickCount();
	for(;;)
	{
		vTaskDelayUntil(&xLastWakeTime,(pdMS_TO_TICKS(BCTASK_RATE)));
		xTickCur=xTaskGetTickCount();
		xTickDelata=xTaskGetTickCount()-xTickOld;
		xTickOld=xTickCur;
		uint32_t delta_tacho0=tacho[0]-old_tacho[0];
		uint32_t delta_tacho1=tacho[1]-old_tacho[1];
		old_tacho[0]=tacho[0];
		old_tacho[1]=tacho[1];
		bc_meters[BC_METER_RPM1]=(float)TachoToRPM(delta_tacho0,xTickDelata);
		bc_meters[BC_METER_RPM2]=(float)TachoToRPM(delta_tacho1,xTickDelata);
		bc_dist_delta=MAX(delta_tacho0,delta_tacho1)*config.wheel*M_PI/1e6/config.magnets;
		bc_meters[BC_METER_ODO1]+=bc_dist_delta;
		bc_meters[BC_METER_ODO2]+=bc_dist_delta;
		bc_meters[BC_METER_SPEED]=bc_dist_delta*(3.6e6/xTickDelata);
		bc_meters[BC_METER_ECO]=(bc_meters[BC_METER_ODO1]>0)?adc_meters[ADC_METER_CONSUMPTION]/bc_meters[BC_METER_ODO1]:0;
		if(xSemaphoreTake(xMeterSemaphoreHandle,125)==pdFAIL)
			continue;
		ComMetersCopy(meters,bc_meters,bc_to_meter_map,ARRAYLEN(bc_to_meter_map));
		xSemaphoreGive(xMeterSemaphoreHandle);
	}
}
