/*
* Copyright (C) 2020 Akim Lesovets
* This file is part of bzzz-obc-open <https://gitlab.com/a.lesovets/bzzz-obc-open>.
*
* bzzz-obc-open is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* bzzz-obc-open is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with bzzz-obc-open.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * ButtonsTask.c
 *
 *  Created on: 10 окт. 2020 г.
 *      Author: AKIM
 */

#include "ButtonsTask.h"
#include <FreeRTOS.h>
#include <portmacro.h>
#include <projdefs.h>
#include "task.h"
#include "semphr.h"
#include "FreeRTOS.h"
#include "task.h"
#include "obc_projdefs.h"

extern QueueHandle_t		xBTQueueHandle;

static portTickType xLastWakeTime;
static enum bt_states bt_state[BT_MAX];
static uint8_t bt_time[BT_MAX];
static enum bt_value bt_val[BT_MAX];
static uint8_t bt_ready;

#define BT_LEFT_PORT	PIND
#define BT_LEFT_PIN		(1<<PD5)
#define BT_CENTER_PORT	PIND
#define BT_CENTER_PIN	(1<<PD6)
#define BT_RIGHT_PORT	PIND
#define BT_RIGHT_PIN	(1<<PD7)

BaseType_t ReadBT(enum bt_value*val)
{
	int res=pdFAIL;
	if(xBTQueueHandle!=NULL)
	{
		if (xQueueReceive(xBTQueueHandle,val,10)==pdPASS)
			res=pdPASS;
	}
	return res;
}

uint8_t ButtonRead(buttons_t button)
{
	switch(button)
	{
	case BT_LEFT:
		return !!(BT_LEFT_PORT&BT_LEFT_PIN);
		break;
	case BT_CENTER:
		return !!(BT_CENTER_PORT&BT_CENTER_PIN);
		break;
	case BT_RIGHT:
		return !!(BT_RIGHT_PORT&BT_RIGHT_PIN);
		break;
	default:
		return 1;
	}
}
#define BTTASK_RATE		(100)
#define BT_LONG_DELAY	(700/BTTASK_RATE)
__attribute__ ((OS_main)) void vButtonsTask(void *pvParameters)
{
	for(;;)
	{
		vTaskDelayUntil(&xLastWakeTime,(pdMS_TO_TICKS(BTTASK_RATE)/*333*/));
		bt_ready=0;
		for (int i=0;i<BT_MAX;i++)
		{
			uint8_t bt_tmp=ButtonRead(i);
			switch (bt_state[i]) {
			case UP:
				if(bt_tmp==0)
				{
					bt_state[i]=DN;
					//bt_val[i]=BT_SHORT;
					//bt_ready=1;
				}
				else
					bt_val[i]=BT_REL;
				break;
			case DN:
				if (bt_tmp==0)
				{
					if (++bt_time[i]>BT_LONG_DELAY)
					{
						bt_state[i]=RL;
						bt_time[i]=0;
					}
				}
				else
				{
					bt_state[i]=UP;
					bt_val[i]=BT_SHORT;
					//bt_val[i]=BT_REL;
					bt_time[i]=0;
					bt_ready=1;
				}
				break;
			case RL:
				if (bt_tmp==0)
				{
					bt_val[i]=BT_LONG;
					bt_ready=1;
				}
				else
				{
					bt_state[i]=UP;
					bt_val[i]=BT_REL;
					bt_ready=1;
				}
				break;
			default:
				break;
			}
		}
		if ((bt_ready)&&(xBTQueueHandle!=NULL))
			xQueueOverwrite(xBTQueueHandle,&bt_val);
	}

}
