/*
* Copyright (C) 2020 Akim Lesovets
* This file is part of bzzz-obc-open <https://gitlab.com/a.lesovets/bzzz-obc-open>.
*
* bzzz-obc-open is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* bzzz-obc-open is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with bzzz-obc-open.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * ADCTask.c
 *
 *  Created on: 10 окт. 2020 г.
 *      Author: AKIM
 */
#include "ADCTask.h"
#include <FreeRTOS.h>
#include <portmacro.h>
#include <projdefs.h>
#include <string.h>
#include "task.h"
#include "semphr.h"
#include "lstLib.h"
#include "obc_projdefs.h"
#include "filter.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "maths.h"
#include "utils.h"
#include "avr/pgmspace.h"
#include "obc_common.h"

#define ADC_I_CHAN1	ADCH1
#define ADC_I_CHAN2	ADCH2
#define ADC_V_CHAN	ADCH0

enum eADCch
{
	ADC_CH_V=0,
	ADC_CH_I1,
	ADC_CH_I2,
	ADC_CH_MAX
};

uint8_t PROGMEM adc_to_meter_map[ADC_METER_MAX]={
		[ADC_METER_VOLTAGE]=METER_VOLTAGE,
		[ADC_METER_CURRENT1]=METER_CURRENT1,
		[ADC_METER_CURRENT2]=METER_CURRENT2,
		[ADC_METER_CURRENT]=METER_CURRENT,
		[ADC_METER_CONSUMPTION]=METER_CONSUMPTION,
};

uint16_t ADCRead(uint8_t chan)
{
	ADMUX=(ADMUX&~0xf)|chan;
	ADCSRA|=1<<ADSC;
	while(ADCSRA & (1<<ADSC));
	return ADC;
}

uint16_t ADCToVoltage(int16_t src, config_t *config)
{
	return ((((int32_t)src * config->vbatscale * 33 + ((ADC_RANGE-1) * 5)) / ((ADC_RANGE-1) * config->vbatresdivval)) / config->vbatresdivmultiplier);
}

int32_t ADCTocentiAmps(uint16_t src , uint16_t offset,config_t *config)
{
	int16_t _src=src-offset;
	int32_t millivolts = ((int32_t )_src * ADC_REF) / ADC_RANGE;
	return ((int32_t)millivolts * 1000) / (int16_t)config->ampscale; // current in 0.01A steps
}

#define ADCTASK_RATE	(1000/50)

extern SemaphoreHandle_t xMeterSemaphoreHandle;
extern float meters[];
extern config_t config;
static uint16_t adc_tmp[ADC_CH_MAX]={0,};
float adc_meters[ADC_METER_MAX];

#define IBAT_LPF_FREQ  0.4f
#define VBAT_LPF_FREQ  0.1f
static biquadFilter_t adciBatFilter1;
static biquadFilter_t adciBatFilter2;
static biquadFilter_t adcVBatFilter;
uint16_t ampoffset[2];

#define I_BAT_DEADBAND	(8)

__attribute__ ((OS_main)) void vADCTask(void *param)
{
	portTickType xLastWakeTime;
	portTickType xTickCur;
	portTickType xTickOld;
	portTickType xTickDelata;
	DIDR0=0xf<<ADC0D;
	ADMUX=(1<<REFS0)|ADCH0;
	ADCSRA=(7<<ADPS0)|(1<<ADEN);
	ADCSRB=0;
	biquadFilterInitLPF(&adcVBatFilter, VBAT_LPF_FREQ, 50000); //50HZ Update
	biquadFilterInitLPF(&adciBatFilter1, IBAT_LPF_FREQ, 50000); //50HZ Update
	biquadFilterInitLPF(&adciBatFilter2, IBAT_LPF_FREQ, 50000); //50HZ Update


	if(config.ampoffset1==0xffff)
	{
		ampoffset[0]=ADCRead(ADC_I_CHAN1);
		for (int i=0;i<1000;i++)
		{
			ampoffset[0]+=ADCRead(ADC_I_CHAN1);
			ampoffset[0]>>=1;
		}
	}
	else
		ampoffset[0]=config.ampoffset1;
	if(config.ampoffset2==0xffff)
	{
		ampoffset[1]=ADCRead(ADC_I_CHAN1);
		for (int i=0;i<1000;i++)
		{
			ampoffset[1]+=ADCRead(ADC_I_CHAN2);
			ampoffset[1]>>=1;
		}
	}
	else
		ampoffset[1]=config.ampoffset2;

	xLastWakeTime = xTaskGetTickCount();
	xTickOld=xLastWakeTime;
	for(;;)
	{
		vTaskDelayUntil(&xLastWakeTime,(ADCTASK_RATE));
		xTickCur=xTaskGetTickCount();
		xTickDelata=xTaskGetTickCount()-xTickOld;
		xTickOld=xTickCur;
		adc_tmp[ADC_CH_V]=ADCRead(ADC_CH_V);
		adc_tmp[ADC_CH_I1]=ADCRead(ADC_CH_I1);
		adc_tmp[ADC_CH_I2]=ADCRead(ADC_CH_I2);
		adc_meters[ADC_METER_CONSUMPTION]+=(float)(applyDeadband(ADCTocentiAmps(adc_tmp[ADC_CH_I1],config.ampoffset1,&config),I_BAT_DEADBAND)+
				applyDeadband(ADCTocentiAmps(adc_tmp[ADC_CH_I2],config.ampoffset2,&config),I_BAT_DEADBAND))*xTickDelata/(100.0f*configTICK_RATE_HZ*3600);
		adc_meters[ADC_METER_VOLTAGE]=(float)biquadFilterApply(&adcVBatFilter,ADCToVoltage(adc_tmp[ADC_CH_V],&config)/10.0f);
		adc_meters[ADC_METER_CURRENT1]=(float)biquadFilterApply(&adciBatFilter1,ADCTocentiAmps(adc_tmp[ADC_CH_I1],config.ampoffset1,&config)/100.0);
		adc_meters[ADC_METER_CURRENT2]=(float)biquadFilterApply(&adciBatFilter2,ADCTocentiAmps(adc_tmp[ADC_CH_I2],config.ampoffset2,&config)/100.0);
		adc_meters[ADC_METER_CURRENT]=adc_meters[ADC_METER_CURRENT1]+adc_meters[ADC_METER_CURRENT2];
		if(xSemaphoreTake(xMeterSemaphoreHandle,10)==pdFAIL)
			continue;
		ComMetersCopy(meters,adc_meters,adc_to_meter_map,ARRAYLEN(adc_to_meter_map));
		meters[METER_POWER1]=adc_meters[ADC_METER_VOLTAGE]*adc_meters[ADC_METER_CURRENT1];
		meters[METER_POWER2]=adc_meters[ADC_METER_VOLTAGE]*adc_meters[ADC_METER_CURRENT2];
		meters[METER_POWER]=adc_meters[ADC_METER_VOLTAGE]*adc_meters[ADC_METER_CURRENT];
		xSemaphoreGive(xMeterSemaphoreHandle);
	}
}
