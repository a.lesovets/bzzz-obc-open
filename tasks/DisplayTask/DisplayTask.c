/*
* Copyright (C) 2020 Akim Lesovets
* This file is part of bzzz-obc-open <https://gitlab.com/a.lesovets/bzzz-obc-open>.
*
* bzzz-obc-open is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* bzzz-obc-open is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with bzzz-obc-open.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * DisplayTask.c
 *
 *  Created on: 10 окт. 2020 г.
 *      Author: AKIM
 */

#include "DisplayTask.h"
#include <FreeRTOS.h>
#include <portmacro.h>
#include <projdefs.h>
#include <string.h>
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "lstLib.h"
#include "obc_projdefs.h"
#include "u8g2.h"
#include "u8x8_avr.h"
#include "KS0108.h"
#include "stdio.h"
#include "util/delay.h"
#include "tasks/ButtonsTask/ButtonsTask.h"
#include "utils.h"
#include "maths.h"
#include "u8x8.h"
#include "avr/eeprom.h"

extern config_t config;

extern const uint8_t JetSet_32[] U8G2_FONT_SECTION("JetSet_32");
extern const uint8_t unifont[] U8G2_FONT_SECTION("unifont");
extern const uint8_t FixedMedium_6[] U8G2_FONT_SECTION("FixedMedium_6");

uint8_t PROGMEM meter_prefixes[METER_MAX]={0,0,0,0,0,0,0,1/*10*/,2/*11*/,12,0,1,2,1,2,1,2};
uint8_t PROGMEM meter_default_accuracy[METER_MAX]={
		[METER_EMPTY]=ITEM_ACCURACY_0,
		[METER_VOLTAGE]=ITEM_ACCURACY_1,
		[METER_CURRENT]=ITEM_ACCURACY_2,
		[METER_POWER]=ITEM_ACCURACY_1,
		[METER_SPEED]=ITEM_ACCURACY_1,
		[METER_CONSUMPTION]=ITEM_ACCURACY_1,
		[METER_ECO]=ITEM_ACCURACY_1,
		[METER_ODO1]=ITEM_ACCURACY_1,
		[METER_ODO2]=ITEM_ACCURACY_1,
		[METER_RANGE]=ITEM_ACCURACY_0,
		[METER_THROTTLE]=ITEM_ACCURACY_0,
		[METER_CURRENT1]=ITEM_ACCURACY_2,
		[METER_CURRENT2]=ITEM_ACCURACY_2,
		[METER_POWER1]=ITEM_ACCURACY_1,
		[METER_POWER2]=ITEM_ACCURACY_1,
		[METER_RPM1]=ITEM_ACCURACY_0,
		[METER_RPM2]=ITEM_ACCURACY_0,
};

char PROGMEM unit_voltage_str[]="В";
char PROGMEM unit_current_str[]="А";
char PROGMEM unit_power_str[]="Вт";
char PROGMEM unit_speed_str[]="км/ч";
char PROGMEM unit_consumption_str[]="Ач";
char PROGMEM unit_eco_str[]="Ач/км";
char PROGMEM unit_dist_str[]="км";
char PROGMEM unit_throttle_str[]="%";
char PROGMEM unit_rpm_str[]="об/мин";
char PROGMEM meter_format_value[]="%.1f";
char PROGMEM meter_format_uints[]="%S";
char PROGMEM unts_delimiter[]="/";
char PROGMEM item_prefixes[]="❶❷❸❹❺❻❼❽❾ⒶⒷⓇ";//XXX: do not edit
char PROGMEM format_u[]="%u";
char PROGMEM next_str[]=">";

PGM_P PROGMEM meter_units[METER_MAX]={unit_voltage_str,unit_current_str,unit_power_str,unit_speed_str,unit_consumption_str,
		unit_eco_str,unit_dist_str,unit_dist_str,unit_dist_str,unit_throttle_str,unit_current_str,
		unit_current_str,unit_power_str,unit_power_str,unit_rpm_str,unit_rpm_str,/*unit_temperature_str*/};

uint8_t PROGMEM default_main_screen[]={0,0,METER_ODO1,ITEM_PROP(ITEM_SIZE_SMALL,ITEM_ACCURACY_1,ITEM_CHANGEABLE_OFF),ITEM_JUST(ITEM_H_JUSTIFY_LEFT,ITEM_V_JUSTIFY_TOP),
		127,0,METER_CURRENT,ITEM_PROP(ITEM_SIZE_SMALL,ITEM_ACCURACY_2,ITEM_CHANGEABLE_OFF),ITEM_JUST(ITEM_H_JUSTIFY_RIGHT,ITEM_V_JUSTIFY_TOP),
		64,32,METER_RPM2,ITEM_PROP(ITEM_SIZE_BIG,ITEM_ACCURACY_0,ITEM_CHANGEABLE_ON),ITEM_JUST(ITEM_H_JUSTIFY_CENTER,ITEM_V_JUSTIFY_CENTER),
		0,63,METER_SPEED,ITEM_PROP(ITEM_SIZE_SMALL,ITEM_ACCURACY_1,ITEM_CHANGEABLE_OFF),ITEM_JUST(ITEM_H_JUSTIFY_LEFT,ITEM_V_JUSTIFY_BOTTOM),
		127,63,METER_VOLTAGE,ITEM_PROP(ITEM_SIZE_SMALL,ITEM_ACCURACY_1,ITEM_CHANGEABLE_OFF),ITEM_JUST(ITEM_H_JUSTIFY_RIGHT,ITEM_V_JUSTIFY_BOTTOM)};

uint8_t PROGMEM default_main_screen2[]={0,0,METER_VOLTAGE,ITEM_PROP(ITEM_SIZE_SMALL,ITEM_ACCURACY_1,ITEM_CHANGEABLE_OFF),ITEM_JUST(ITEM_H_JUSTIFY_LEFT,ITEM_V_JUSTIFY_TOP),
		0,12,METER_CURRENT1,ITEM_PROP(ITEM_SIZE_SMALL,ITEM_ACCURACY_2,ITEM_CHANGEABLE_OFF),ITEM_JUST(ITEM_H_JUSTIFY_LEFT,ITEM_V_JUSTIFY_TOP),
		0,24,METER_CURRENT2,ITEM_PROP(ITEM_SIZE_SMALL,ITEM_ACCURACY_2,ITEM_CHANGEABLE_OFF),ITEM_JUST(ITEM_H_JUSTIFY_LEFT,ITEM_V_JUSTIFY_TOP),
		0,36,METER_CURRENT,ITEM_PROP(ITEM_SIZE_SMALL,ITEM_ACCURACY_2,ITEM_CHANGEABLE_OFF),ITEM_JUST(ITEM_H_JUSTIFY_LEFT,ITEM_V_JUSTIFY_TOP),
		0,48,METER_CONSUMPTION,ITEM_PROP(ITEM_SIZE_SMALL,ITEM_ACCURACY_1,ITEM_CHANGEABLE_OFF),ITEM_JUST(ITEM_H_JUSTIFY_LEFT,ITEM_V_JUSTIFY_TOP),
		64,24,METER_POWER1,ITEM_PROP(ITEM_SIZE_SMALL,ITEM_ACCURACY_1,ITEM_CHANGEABLE_OFF),ITEM_JUST(ITEM_H_JUSTIFY_LEFT,ITEM_V_JUSTIFY_TOP),
		64,36,METER_POWER2,ITEM_PROP(ITEM_SIZE_SMALL,ITEM_ACCURACY_1,ITEM_CHANGEABLE_OFF),ITEM_JUST(ITEM_H_JUSTIFY_LEFT,ITEM_V_JUSTIFY_TOP),
		64,48,METER_POWER,ITEM_PROP(ITEM_SIZE_SMALL,ITEM_ACCURACY_1,ITEM_CHANGEABLE_OFF),ITEM_JUST(ITEM_H_JUSTIFY_LEFT,ITEM_V_JUSTIFY_TOP),};

static item_t item_pool[ITEMS_NUM]={0,};
static LIST free_items ={0,};

static screen_t screen_pool[SCREENS_NUM]={0,};
static LIST free_screens ={0,};
static LIST screen_list={0,};
static screen_t *screen=NULL;

static u8g2_t u8g2;
static enum bt_value buttons[BT_MAX];
static uint8_t bt_release=0;
static uint8_t edit_mode=0;
static uint16_t ampoffset_tmp=0;

void DrawMenu(void);
void DrawDesktop(void);
void DrawStats(void);
int8_t MenuSetEdit(void);
void (*DrawFunction)()=&DrawDesktop;

extern float meters[METER_MAX];
extern eestore_t EEMEM eestore;

void ItemListInit(void)
{
	lstInit(&free_items);
	for (int i =0;i<ITEMS_NUM;i++)
	{
		ITEM_CLEAR(&item_pool[i]);
		lstAdd(&free_items,(NODE*)&item_pool[i]);
	}
}

void ScreenListInit(void)
{
	lstInit(&free_screens);
	lstInit(&screen_list);
	for (int i =0;i<SCREENS_NUM;i++)
	{
		SCREEN_CLEAR(&screen_pool[i]);
		lstAdd(&free_screens,(NODE*)&screen_pool[i]);
	}
}

int8_t ScreenDelete(void)
{
	if(screen==NULL) return 1;
	lstDelete(&screen_list,&screen->node);
	lstAdd(&free_screens,&screen->node);
	item_t *_item=(item_t*)lstGet(&screen->items);
	if(_item==NULL) return 1;
	do{
		lstAdd(&free_items,&_item->node);
		_item=(item_t*)lstGet(&screen->items);
	}while(_item!=NULL);
	screen=(screen_t*)lstFirst(&screen_list);
	return 1;
}

void MenuDrawStats(void)
{
	DrawFunction=&DrawStats;
}

void ConfigSave(void)
{
	eeprom_write_dword(&eestore.cfg_magic,CFG_MAGIC);
	eeprom_write_block(&config,&eestore.cfg,sizeof(config));
}

void ScreenSave(void)
{
	void* p=(void*)eestore.scrn_data;
	screen_t *_scrn=(screen_t*)lstFirst(&screen_list);
	if(_scrn==NULL) return;
	do
	{
		item_t *_item=(item_t*)lstFirst(&_scrn->items);
		if(_item==NULL)break;
		eeprom_write_dword(p,SCRN_MAGIC);
		p+=4;
		do
		{
//			eeprom_busy_wait();//XXX Opt
			eeprom_write_block(&_item->params,p,sizeof(_item->params));
			p+=sizeof(_item->params);
			_item=(item_t*)lstNext(&_item->node);
		}while(_item!=NULL);
		_scrn=(screen_t*)lstNext(&_scrn->node);
	}while(_scrn!=NULL);
//	eeprom_busy_wait();//XXX Opt
	eeprom_write_dword(p,SCRN_END);
}

int8_t AddScreen(void);
int8_t AddDefaultScreen(uint8_t *default_screen,uint8_t size);
void ScreenRestore(void)
{
	void* p=(void*)eestore.scrn_data;
	uint32_t magic;
	eeprom_read_block(&magic,p,sizeof(magic));
	if(magic!=SCRN_MAGIC)
	{
		AddDefaultScreen(default_main_screen,(sizeof(default_main_screen)/sizeof(item_params_t)));
		ScreenSave();
		return;
	}
	p+=sizeof(magic);
//	if(AddScreen()!=0) return;
	AddScreen();//XXX Opt
	for(;;)
	{
		eeprom_read_block(&magic,p,sizeof(magic));
		if(magic==SCRN_MAGIC)
		{
			if(AddScreen()!=0) break;
			p+=sizeof(magic);
		}
		else if(magic==SCRN_END) break;
		item_t* _item=(item_t *)lstGet(&free_items);
		if(_item==NULL) break;
		eeprom_read_block(&_item->params,p,sizeof(_item->params));
		_item->select=ITEM_SELECT_NOSELECT;
		lstAdd(&screen->items,&_item->node);
		p+=sizeof(_item->params);
	}
	screen=(screen_t*)lstFirst(&screen_list);
}

BaseType_t DispReadBT(void)
{
	BaseType_t res=ReadBT(buttons);
	if (bt_release!=0)
	{
		if (res==pdPASS)
		{
			bt_release=0;
			for(uint8_t b=0;b<BT_MAX;b++)
				bt_release+=buttons[b];
		}
	}
	return res;
}


void DrawItem(item_t *item)
{
	char str_prefix[4];
	char str_val[9];
	char str_unit[17];
	char str_meter_format[5];
	item_params_t *_item=&item->params;
	u8g2_uint_t val_w=0,val_h=0,unit_w=0,unit_h=0,pref_w=0,pref_h=0;
	u8g2_int_t val_des=0,dx=0,pref_delta_y=0,val_delta_y=0,pref_des=0,
			unit_des=0,unit_delta_y=0,frame_delta_y=0,frame_delta_h=0;
	u8g2_uint_t unit_x_offs=0;
	uint8_t *font_pref;
	uint8_t *font_val;
	uint8_t *font_unit;
	uint8_t pref;

	if(_item->meter==METER_EMPTY) return;
	if(_item->size==ITEM_SIZE_BIG)
	{
		font_pref=(uint8_t*)ITEM_BIG_PREFIX;
		font_val=(uint8_t*)ITEM_BIG_FONT;
		font_unit=(uint8_t*)ITEM_BIG_UNIT;
		unit_x_offs=0;
		frame_delta_y=-1;
		frame_delta_h=3;
	}
	else
	{
		font_pref=(uint8_t*)ITEM_SMALL_PREFIX;
		font_val=(uint8_t*)ITEM_SMALL_VAL;
		font_unit=(uint8_t*)ITEM_SMALL_UNIT;
		unit_x_offs=1;
	}

	u8g2_SetFont(&u8g2, font_pref);
	pref_h=u8g2_GetAscent(&u8g2);
	memset(str_prefix,0,sizeof(str_prefix));
	pref=pgm_read_byte(&meter_prefixes[_item->meter]);
	if(pref!=0)
	{
		memcpy_P(str_prefix,&item_prefixes[(pref-1)*3],3);
		pref_w=u8g2_GetUTF8Width(&u8g2,str_prefix);
	}
	pref_des=u8g2_GetDescent(&u8g2);

	strcpy_P(str_meter_format,meter_format_value);
	str_meter_format[METER_FORMAT_ACC_POS]=NUM(_item->accuracy);
	sprintf(str_val,str_meter_format,meters[_item->meter]);
	u8g2_SetFont(&u8g2, font_val);
	val_w=u8g2_GetUTF8Width(&u8g2,str_val);
	val_h=u8g2_GetAscent(&u8g2);
	val_des=u8g2_GetDescent(&u8g2);

	sprintf_P(str_unit,meter_format_uints,pgm_read_word(&meter_units[_item->meter-1]));
	strtok_P(str_unit,unts_delimiter);
	u8g2_SetFont(&u8g2, font_unit);
	char*p2=strtok_P(NULL,unts_delimiter);
	unit_w=u8g2_GetUTF8Width(&u8g2,str_unit);
	if(p2!=NULL)
		unit_w=MAX(unit_w,u8g2_GetUTF8Width(&u8g2,p2));
	unit_h=u8g2_GetAscent(&u8g2);
	unit_des=u8g2_GetDescent(&u8g2);

	if(_item->vjust==ITEM_V_JUSTIFY_TOP)
	{
		u8g2_SetFontPosTop(&u8g2);
		pref_delta_y=val_h-pref_h;
		val_delta_y=0;
		unit_delta_y=val_h-unit_h;
		frame_delta_y+=0;
		frame_delta_h+=-val_des;
	}
	else if(_item->vjust==ITEM_V_JUSTIFY_BOTTOM)
	{
		u8g2_SetFontPosBottom(&u8g2);
		pref_delta_y=-pref_des;
		val_delta_y=-val_des;
		unit_delta_y=-unit_des;
		frame_delta_y+=-val_h-val_delta_y+1;
		frame_delta_h+=-val_des;
	}
	else if(_item->vjust==ITEM_V_JUSTIFY_CENTER)
	{
		u8g2_SetFontPosCenter(&u8g2);
		pref_delta_y=(val_h-unit_h)/2-pref_des;
		val_delta_y+=0;
		unit_delta_y=(val_h-unit_h)/2-unit_des;
		frame_delta_y+=-(val_h-val_des)/2;
		frame_delta_h+=-val_des;
	}
	if(_item->hjust==ITEM_H_JUSTIFY_LEFT)
		dx=1;
	else if(_item->hjust==ITEM_H_JUSTIFY_RIGHT)
		dx=-(pref_w+val_w+unit_w)-1;
	else if(_item->hjust==ITEM_H_JUSTIFY_CENTER)
		dx=-(pref_w+val_w+unit_w)/2;

	dx+=_item->x;

	u8g2_SetFont(&u8g2, font_pref);
	u8g2_DrawUTF8(&u8g2,dx,_item->y+pref_delta_y,str_prefix);

	u8g2_SetFont(&u8g2, font_val);
	u8g2_DrawUTF8(&u8g2,dx+pref_w,_item->y+val_delta_y,str_val);

	u8g2_SetFont(&u8g2, font_unit);
	unit_delta_y+=_item->y;
	val_w+=pref_w;
	if(p2==NULL)
		u8g2_DrawUTF8(&u8g2,dx+val_w+unit_x_offs,unit_delta_y,str_unit);
	else
	{
		u8g2_DrawUTF8(&u8g2,dx+val_w+unit_x_offs,unit_delta_y,p2);

		if(_item->size==ITEM_SIZE_BIG)
		{
			u8g2_DrawUTF8(&u8g2,dx+val_w+(unit_x_offs++),unit_delta_y-unit_h-4,str_unit);
			u8g2_DrawHLine(&u8g2,dx+val_w+unit_x_offs,_item->y+(val_h/2-unit_h),unit_w);
			u8g2_DrawHLine(&u8g2,dx+val_w+unit_x_offs,_item->y+(val_h/2-unit_h)+1,unit_w);
		}
		else
			u8g2_DrawUTF8(&u8g2,dx+val_w+unit_x_offs,unit_delta_y-unit_h,str_unit);
	}

	unit_w+=val_w+3;
	frame_delta_y+=_item->y;
	frame_delta_h+=val_h;
	if(item->select!=ITEM_SELECT_NOSELECT)
	{
		u8g2_DrawFrame(&u8g2,dx-1,frame_delta_y,unit_w,frame_delta_h);
	}
	frame_delta_h-=2;
	frame_delta_y++;
	if(item->select==ITEM_SELECT_HALF)
		u8g2_DrawBox(&u8g2,dx,frame_delta_y,unit_w/2,frame_delta_h);
	else if (item->select==ITEM_SELECT_FULL)
		u8g2_DrawBox(&u8g2,dx,frame_delta_y,unit_w-2,frame_delta_h);
}

void DrawScreen(void)
{
	if(screen==NULL) return;
	item_t *_item=(item_t*)lstFirst(&screen->items);
	if(_item==NULL) return;
	do
	{
		DrawItem(_item);
		_item=(item_t*)lstNext(&_item->node);
	}while (_item!=NULL);
}

int8_t AddScreen(void)
{
	screen=(screen_t*)lstGet(&free_screens);
	if(screen==NULL) return -1;
	lstAdd(&screen_list,&screen->node);
	return 0;
}

int8_t AddDefaultScreen(uint8_t *default_screen,uint8_t size)
{
	if(AddScreen()==-1) return -1;
	item_params_t *par=(item_params_t*)default_screen;
	for(uint8_t i=0;i<size;i++,par++)
	{
		item_t *_item=(item_t*)lstGet(&free_items);
		if(_item==NULL) return -2;
		memcpy_P(&_item->params,par,sizeof(item_params_t));
		_item->select=ITEM_SELECT_NOSELECT;
		lstAdd(&screen->items,&_item->node);
	}
	return 0;
}

int8_t MenuAddScreen1(void)
{
	if(AddDefaultScreen(default_main_screen,(sizeof(default_main_screen)/sizeof(item_params_t)))==-1) return -1;
	MenuSetEdit();
	return 0;
}
int8_t MenuAddScreen2(void)
{
	if(AddDefaultScreen(default_main_screen2,(sizeof(default_main_screen2)/sizeof(item_params_t)))==-1) return -1;
	MenuSetEdit();
	return 0;
}

static PGM_P edit_name;
static uint16_t *edit_param;
static uint16_t tmp_param;
static uint16_t edit_meter;
#define LONG_INC_DEF	(5)
extern uint16_t ampoffset[];
void MenuEditParam(void)
{
	char str[32];
	static uint16_t long_inc=LONG_INC_DEF;
	u8g2_SetFont(&u8g2, ITEM_SMALL_VAL);
	u8g2_SetFontPosTop(&u8g2);
	uint8_t h=u8g2_GetAscent(&u8g2) - u8g2_GetDescent(&u8g2);
	strcpy_P(str,(PGM_P)pgm_read_word(edit_name));
	u8g2_DrawBox(&u8g2,0,0,u8g2_GetDisplayWidth(&u8g2),h);
	uint8_t w=u8g2_GetUTF8Width(&u8g2,str);
	u8g2_DrawUTF8(&u8g2,(u8g2_GetDisplayWidth(&u8g2)-w)/2,0,str);
	BaseType_t res=DispReadBT();

	if ((res==pdPASS)&&(bt_release==0))
	{
		if ((buttons[BT_RIGHT]&BT_LONG)==BT_SHORT)
		{
			if(((int32_t)(*edit_param)+1)<=65535)
				*edit_param+=1;

		}
		else if ((buttons[BT_LEFT]&BT_LONG)==BT_SHORT)
		{
			if((((int32_t)(*edit_param))-1)>=0)
				*edit_param-=1;
		}
		else if ((buttons[BT_CENTER]&BT_LONG)==BT_SHORT)
		{
			ConfigSave();

			DrawFunction=&DrawMenu;
		}
		else if ((buttons[BT_RIGHT]&BT_LONG)==BT_LONG)
		{
			if(((int32_t)(*edit_param)+long_inc)<=(65535-long_inc))
				*edit_param+=long_inc;
			else
				*edit_param=65535;
			long_inc+=1;
		}
		else if ((buttons[BT_LEFT]&BT_LONG)==BT_LONG)
		{
			if((((int32_t)(*edit_param))-long_inc)>=long_inc)
				*edit_param-=long_inc;
			else
				*edit_param=0;
			long_inc+=1;
		}
		else
			long_inc=LONG_INC_DEF;
	}
	if((edit_param==&config.ampoffset1)||(edit_param==&config.ampoffset2))
	{
		uint8_t amp_idx=edit_param==&config.ampoffset1?0:1;
		ampoffset[amp_idx]=(*edit_param==0xffff)?ampoffset_tmp:*edit_param;
	}

	sprintf_P(str,PSTR("<%u>"),*edit_param);
	w=u8g2_GetUTF8Width(&u8g2,str);
	u8g2_DrawUTF8(&u8g2,(u8g2_GetDisplayWidth(&u8g2)-w)/2,20,str);
	item_t *_item=(item_t*)lstGet(&free_items);
	if (_item==NULL) return;
	_item->params.x=u8g2_GetDisplayWidth(&u8g2)/2;
	_item->params.y=40;
	_item->params.meter=edit_meter;
	_item->params.size=ITEM_SIZE_SMALL;
	_item->params.accuracy=pgm_read_byte(&meter_default_accuracy[edit_meter]);
	_item->params.changeable=ITEM_CHANGEABLE_OFF;
	_item->params.hjust=ITEM_H_JUSTIFY_CENTER;
	_item->params.vjust=ITEM_V_JUSTIFY_TOP;
	_item->select=ITEM_SELECT_NOSELECT;
	DrawItem(_item);
	lstAdd(&free_items,&_item->node);
}

const char PROGMEM menu_str[]="Меню";//
const char PROGMEM desktop_str[]="Рабочий стол";//
const char PROGMEM edit_str[]="Редактировать";//
const char PROGMEM add_str[]="Добавить";//
const char PROGMEM delete_str[]="Удалить";//
const char PROGMEM reset_str[]="Сброс";//
const char PROGMEM settings_str[]="Настройки";//
const char PROGMEM wheel_str[]="Колесо";//
const char PROGMEM voltage_str[]="Напряжение";//
const char PROGMEM scale_str[]="Масштаб";//
const char PROGMEM div_str[]="Делитель";//
const char PROGMEM mult_str[]="Множитель";//
const char PROGMEM current_str[]="Ток";//
const char PROGMEM offs1_str[]="Сдвиг 1";
const char PROGMEM offs2_str[]="Сдвиг 2";
const char PROGMEM offs_auto_str[]="АВТО";
const char PROGMEM magnets_str[]="Магниты";//
const char PROGMEM battery_str[]="АКБ";//
const char PROGMEM stat_str[]="Статистика";//
const char PROGMEM type1_str[]="Вид 1";//
const char PROGMEM type2_str[]="Вид 2";//

enum eMenuItemType
{
	MENU_ITEM_NULL=0,
	MENU_ITEM_NEXT,
	MENU_ITEM_PARAM,
	MENU_ITEM_FUNC,
	MENU_ITEM_DRAWFUNC,
};

typedef struct sMenuItem
{
	struct sMenu *next;
	void *action;
	PGM_P text;
	uint8_t type;
}MenuItem_t;

typedef struct sMenu
{
	PGM_P name;
	uint8_t len;
	PGM_P items[];
}Menu_t;

Menu_t PROGMEM MenuNull={0,0,{0}};
#define MAKE_MENU_ITEM(name,next,action,text,type)\
	extern Menu_t next;\
	MenuItem_t PROGMEM name={(void*)&next,(void*)action,text,type}

enum eMenuParams
{
	MENU_PARAM_NULL=0,
	MENU_PARAM_VSCALE,
	MENU_PARAM_VDIV,
	MENU_PARAM_VMULT,
	MENU_PARAM_ISCALE,
	MENU_PARAM_IOFFS1,
	MENU_PARAM_IOFFS2,
	MENU_PARAM_WHEEL,
	MENU_PARAM_MAGNET,
	MENU_PARAM_BATTERY,
	MENU_PARAM_MAX
};

PROGMEM uint16_t* menu_param_map[MENU_PARAM_MAX]={
		[MENU_PARAM_NULL]=0,
		[MENU_PARAM_VSCALE]=&config.vbatscale,
		[MENU_PARAM_VDIV]=&config.vbatresdivval,
		[MENU_PARAM_VMULT]=&config.vbatresdivmultiplier,
		[MENU_PARAM_ISCALE]=&config.ampscale,
		[MENU_PARAM_IOFFS1]=&config.ampoffset1,
		[MENU_PARAM_IOFFS2]=&config.ampoffset2,
		[MENU_PARAM_WHEEL]=&config.wheel,
		[MENU_PARAM_MAGNET]=&config.magnets,
		[MENU_PARAM_BATTERY]=&config.battery,
};

PROGMEM uint8_t edit_meter_map[MENU_PARAM_MAX]={
		[MENU_PARAM_NULL]=METER_EMPTY,
		[MENU_PARAM_VSCALE]=METER_VOLTAGE,
		[MENU_PARAM_VDIV]=METER_VOLTAGE,
		[MENU_PARAM_VMULT]=METER_VOLTAGE,
		[MENU_PARAM_ISCALE]=METER_CURRENT1,
		[MENU_PARAM_IOFFS1]=METER_CURRENT1,
		[MENU_PARAM_IOFFS2]=METER_CURRENT2,
		[MENU_PARAM_WHEEL]=METER_SPEED,
		[MENU_PARAM_MAGNET]=METER_SPEED,
		[MENU_PARAM_BATTERY]=METER_EMPTY,
};
int8_t MenuDesktopReset(void);
int8_t ConfigReset(void);

MAKE_MENU_ITEM(menu_item_desktop,DesktopMenu,NULL,desktop_str,MENU_ITEM_NEXT);//++
	MAKE_MENU_ITEM(menu_item_edit,MenuNull,&MenuSetEdit,edit_str,MENU_ITEM_FUNC);//
	MAKE_MENU_ITEM(menu_item_add,AddMenu,NULL,add_str,MENU_ITEM_NEXT);//
		MAKE_MENU_ITEM(menu_item_type1,MenuNull,&MenuAddScreen1,type1_str,MENU_ITEM_FUNC);//
		MAKE_MENU_ITEM(menu_item_type2,MenuNull,&MenuAddScreen2,type2_str,MENU_ITEM_FUNC);//
	MAKE_MENU_ITEM(menu_item_delete,MenuNull,&ScreenDelete,delete_str,MENU_ITEM_FUNC);//
	MAKE_MENU_ITEM(menu_item_desktop_reset,MenuNull,&MenuDesktopReset,reset_str,MENU_ITEM_FUNC);//
MAKE_MENU_ITEM(menu_item_settings,SettingsMenu,NULL,settings_str,MENU_ITEM_NEXT);//
	MAKE_MENU_ITEM(menu_item_voltage,VoltageMenu,NULL,voltage_str,MENU_ITEM_NEXT);//
		MAKE_MENU_ITEM(menu_item_vscale,MenuNull,MENU_PARAM_VSCALE,scale_str,MENU_ITEM_PARAM);//
		MAKE_MENU_ITEM(menu_item_vdiv,MenuNull,MENU_PARAM_VDIV,div_str,MENU_ITEM_PARAM);//
		MAKE_MENU_ITEM(menu_item_vmult,MenuNull,MENU_PARAM_VMULT,mult_str,MENU_ITEM_PARAM);//
	MAKE_MENU_ITEM(menu_item_curr,CurrMenu,NULL,current_str,MENU_ITEM_NEXT);//
		MAKE_MENU_ITEM(menu_item_iscale,MenuNull,MENU_PARAM_ISCALE,scale_str,MENU_ITEM_PARAM);
		MAKE_MENU_ITEM(menu_item_ioffs1,MenuNull,MENU_PARAM_IOFFS1,offs1_str,MENU_ITEM_PARAM);
		MAKE_MENU_ITEM(menu_item_ioffs2,MenuNull,MENU_PARAM_IOFFS2,offs2_str,MENU_ITEM_PARAM);
	MAKE_MENU_ITEM(menu_item_wheel,MenuNull,MENU_PARAM_WHEEL,wheel_str,MENU_ITEM_PARAM);//
	MAKE_MENU_ITEM(menu_item_magnets,MenuNull,MENU_PARAM_MAGNET,magnets_str,MENU_ITEM_PARAM);//
	MAKE_MENU_ITEM(menu_item_battery,MenuNull,MENU_PARAM_BATTERY,battery_str,MENU_ITEM_PARAM);//
	MAKE_MENU_ITEM(menu_item_config_reset,MenuNull,&ConfigReset,reset_str,MENU_ITEM_FUNC);//
MAKE_MENU_ITEM(menu_item_stat,MenuNull,&DrawStats,stat_str,MENU_ITEM_DRAWFUNC);//

Menu_t PROGMEM MainMenu={
		.name=menu_str,
		.len=3,
		.items={
				(PGM_P)&menu_item_desktop,
				(PGM_P)&menu_item_settings,
				(PGM_P)&menu_item_stat,
		}
};

Menu_t PROGMEM DesktopMenu={
		.name=desktop_str,
		.len=4,
		.items={
				(PGM_P)&menu_item_edit,
				(PGM_P)&menu_item_add,
				(PGM_P)&menu_item_delete,
				(PGM_P)&menu_item_desktop_reset,
		}
};

Menu_t PROGMEM AddMenu={
		.name=add_str,
		.len=2,
		.items={
				(PGM_P)&menu_item_type1,
				(PGM_P)&menu_item_type2,
		}
};

Menu_t PROGMEM SettingsMenu={
		.name=settings_str,
		.len=6,
		.items={
				(PGM_P)&menu_item_voltage,
				(PGM_P)&menu_item_curr,
				(PGM_P)&menu_item_wheel,
				(PGM_P)&menu_item_magnets,
				(PGM_P)&menu_item_battery,
				(PGM_P)&menu_item_config_reset,
		}
};

Menu_t PROGMEM VoltageMenu={
		.name=voltage_str,
		.len=3,
		.items={
				(PGM_P)&menu_item_vscale,
				(PGM_P)&menu_item_vdiv,
				(PGM_P)&menu_item_vmult,
		}
};

Menu_t PROGMEM CurrMenu={
		.name=current_str,
		.len=3,
		.items={
				(PGM_P)&menu_item_iscale,
				(PGM_P)&menu_item_ioffs1,
				(PGM_P)&menu_item_ioffs2,
		}
};

#define MENU_PAGE_LEN	(4)
#define MENU_MAX_STACK	(3)

void DrawMenu(void)
{
	char str[32];
	static int8_t idx=0;
	static int8_t depth=0;
	static Menu_t *cur_menu=(void*)&MainMenu;
	static Menu_t *menu_stack[MENU_MAX_STACK];
	static int8_t idx_stack[MENU_MAX_STACK];
	uint8_t menu_len=pgm_read_byte(&cur_menu->len);
	BaseType_t res=DispReadBT();

	if ((res==pdPASS)&&(bt_release==0))
	{
		if ((buttons[BT_RIGHT]&BT_LONG)==BT_SHORT)
		{
			if(++idx>=menu_len) idx=menu_len-1;
		}
		else if ((buttons[BT_LEFT]&BT_LONG)==BT_SHORT)
		{
			if(--idx<0) idx=0;
		}
		else if ((buttons[BT_LEFT]&BT_LONG)==BT_LONG)
		{
			if(--depth<0)
			{
				DrawFunction=&DrawDesktop;
				goto DRAW_MENU_RESET_RETURN;
			}
			else
			{
				cur_menu=menu_stack[depth];
				idx=idx_stack[depth];
			}
			bt_release=1;
		}
		else if ((buttons[BT_CENTER]&BT_LONG)==BT_SHORT)
		{
			PGM_P item=(PGM_P)pgm_read_word((PGM_P)&(cur_menu->items[idx]));
			uint8_t item_type=pgm_read_byte(&((MenuItem_t*)item)->type);
			if(item_type==MENU_ITEM_NEXT)
			{
				if(depth<MENU_MAX_STACK)
				{
					menu_stack[depth]=cur_menu;
					idx_stack[depth]=idx;
					if(++depth>=MENU_MAX_STACK)
						depth=MENU_MAX_STACK-1;
					cur_menu=(Menu_t*)pgm_read_word(&((MenuItem_t*)item)->next);
					idx=0;
				}
			}
			else if((item_type==MENU_ITEM_FUNC)||(item_type==MENU_ITEM_DRAWFUNC))
			{
				uint16_t ff=pgm_read_word(&((MenuItem_t*)item)->action);

				if(ff!=(uint16_t)NULL)
				{
					int8_t (*f)()=(void*)ff;
					if(item_type==MENU_ITEM_FUNC)
					{
						if (f()==0)
						{
							menu_stack[0]=cur_menu=(void*)&MainMenu;
							idx_stack[0]=0;
							depth=0;
							idx=0;
						}
					}
					else
					{
						DrawFunction=(void*)f;
						menu_stack[0]=cur_menu=(void*)&MainMenu;
						idx_stack[0]=0;
		DRAW_MENU_RESET_RETURN:
						depth=0;
						idx=0;
						return;
					}
				}
			}
			else if(item_type==MENU_ITEM_PARAM)
			{
				edit_name=(char*)&((MenuItem_t*)item)->text;
				edit_param=((uint16_t*)pgm_read_word(&menu_param_map[pgm_read_word(&((MenuItem_t*)item)->action)]));
				edit_meter=pgm_read_byte(&edit_meter_map[pgm_read_word(&((MenuItem_t*)item)->action)]);
				tmp_param=*edit_param;
				if(edit_param==&config.ampoffset1)
					ampoffset_tmp=ampoffset[0];
				else if(edit_param==&config.ampoffset2)
					ampoffset_tmp=ampoffset[1];
				DrawFunction=MenuEditParam;
				return;
			}
		}
	}

	u8g2_SetFont(&u8g2, ITEM_SMALL_VAL);
	u8g2_SetFontPosTop(&u8g2);
	uint8_t h=u8g2_GetAscent(&u8g2) - u8g2_GetDescent(&u8g2);
	strcpy_P(str,(PGM_P)pgm_read_word((PGM_P)&(cur_menu->name)));
	u8g2_DrawBox(&u8g2,0,0,u8g2_GetDisplayWidth(&u8g2),h);
	uint8_t w=u8g2_GetUTF8Width(&u8g2,str);
	u8g2_DrawUTF8(&u8g2,(u8g2_GetDisplayWidth(&u8g2)-w)/2,0,str);

	sprintf_P(str,format_u,uxTaskGetStackHighWaterMark(NULL));
	w=u8g2_GetUTF8Width(&u8g2,str);
	u8g2_DrawUTF8(&u8g2,(u8g2_GetDisplayWidth(&u8g2)-w),0,str);

	menu_len=pgm_read_byte(&cur_menu->len);
	uint8_t hh=h+1;
	uint8_t idx_offs=idx/MENU_PAGE_LEN*MENU_PAGE_LEN;
	for(uint8_t i=0;i<MENU_PAGE_LEN;i++)
	{
		if((i+idx_offs)>=menu_len)
			break;
		if((i+idx_offs)==idx)
		{
			u8g2_DrawBox(&u8g2,0,hh-1,u8g2_GetDisplayWidth(&u8g2),h+1);
		}
		PGM_P item=(PGM_P)pgm_read_word((PGM_P)&(cur_menu->items[i+idx_offs]));
		strcpy_P(str,(PGM_P)pgm_read_word(&((MenuItem_t*)item)->text));
		u8g2_DrawUTF8(&u8g2,0,hh,str);
		uint8_t item_type=pgm_read_byte(&((MenuItem_t*)item)->type);
		if((item_type==MENU_ITEM_NEXT)||(item_type==MENU_ITEM_FUNC))
		{
			sprintf_P(str,meter_format_uints,next_str);
			w=u8g2_GetUTF8Width(&u8g2,str);
			u8g2_DrawUTF8(&u8g2,u8g2_GetDisplayWidth(&u8g2)-w,hh,str);
		}
		else if(item_type==MENU_ITEM_PARAM)
		{
			uint16_t* par=((uint16_t*)pgm_read_word(&menu_param_map[pgm_read_word(&((MenuItem_t*)item)->action)]));
			if (((par==&config.ampoffset1)||(par==&config.ampoffset2))&&(*par==0xffff))
				strcpy_P(str,offs_auto_str);
			else
				sprintf_P(str,format_u,*par);
			w=u8g2_GetUTF8Width(&u8g2,str);
			u8g2_DrawUTF8(&u8g2,u8g2_GetDisplayWidth(&u8g2)-w,hh,str);
		}
		hh+=h+1;
	}
}

enum eEditMode
{
	EDIT_NONE=ITEM_SELECT_NOSELECT,
	EDIT_SELECT=ITEM_SELECT_SELECT,
	EDIT_METER=ITEM_SELECT_HALF,
	EDIT_ACC=ITEM_SELECT_FULL
};

#define EDIT_TO_SELECT(x)	(x)

int8_t MenuSetEdit(void)
{
	if(screen==NULL) return 1;
	edit_mode=EDIT_SELECT;
	DrawFunction=&DrawDesktop;
	return 1;
}

int8_t MenuDesktopReset(void)
{
	ItemListInit();
	ScreenListInit();
	AddDefaultScreen(default_main_screen,(sizeof(default_main_screen)/sizeof(item_params_t)));
	ScreenSave();
	return 0;
}

extern config_t PROGMEM def_config;
int8_t ConfigReset(void)
{
	memcpy_P(&config,&def_config,sizeof(def_config));
	ConfigSave();
	return 1;
}

item_t* GetItemByIdx(uint8_t idx)
{
	uint8_t i=0;
	if(idx>=lstCount(&screen->items)) return NULL;
	item_t *_item=(item_t*)lstFirst(&screen->items);
	if(_item==NULL) return NULL;
	do
	{
		if(idx==i++) return _item;
		_item=(item_t*)lstNext((NODE*)_item);
	}while (_item!=NULL);
	return NULL;
}

void DrawDesktop(void)
{
	screen_t *_scrn=NULL;
	static item_t *_item;
	static int8_t idx=0;
	BaseType_t res=DispReadBT();

	if((edit_mode==EDIT_SELECT)&&(idx==0))
	{
		_item=GetItemByIdx(idx);
		_item->select=EDIT_TO_SELECT(EDIT_SELECT);
	}

	if ((res==pdPASS)&&(bt_release==0))
	{
		if(edit_mode==EDIT_NONE)
		{
			if ((buttons[BT_RIGHT]&BT_LONG)==BT_SHORT)
			{
				_scrn=(screen_t *)lstNext(&screen->node);
				if(_scrn!=NULL)
					screen=_scrn;
			}
			else if((buttons[BT_LEFT]&BT_LONG)==BT_SHORT)
			{
				_scrn=(screen_t *)lstPrevious(&screen->node);
				if(_scrn!=NULL)
					screen=_scrn;
			}
			else if((buttons[BT_CENTER]&BT_LONG)==BT_SHORT)
			{
				_item=(item_t*)lstFirst(&screen->items);
				if(_item==NULL) return;
				do
				{
					if(_item->params.changeable)
					{
						if(++_item->params.meter>=METER_MAX)
							_item->params.meter=1;
						_item->params.accuracy=pgm_read_byte(&meter_default_accuracy[_item->params.meter]);
					}
					_item=(item_t*)lstNext(&_item->node);
				}while (_item!=NULL);
			}
			else if((buttons[BT_CENTER]&BT_LONG)==BT_LONG)
			{
				DrawFunction=&DrawMenu;
				bt_release=1;
				return;
			}
		}
		else
		{
			uint8_t scrn_len=lstCount(&screen->items);
			if(edit_mode==EDIT_SELECT)
			{
				if ((buttons[BT_RIGHT]&BT_LONG)==BT_SHORT)
				{
					if(++idx>=scrn_len)
						idx=0;
				}
				else if ((buttons[BT_LEFT]&BT_LONG)==BT_SHORT)
				{
					if(--idx<0)
						idx=scrn_len-1;
				}
				else if ((buttons[BT_CENTER]&BT_LONG)==BT_SHORT)
				{
					edit_mode=EDIT_METER;
					_item->select=EDIT_TO_SELECT(EDIT_METER);
					goto ITEM_CHANGE_SKIP;
				}

				_item->select=EDIT_TO_SELECT(EDIT_NONE);
				_item=GetItemByIdx(idx);
				_item->select=EDIT_TO_SELECT(EDIT_SELECT);
				ITEM_CHANGE_SKIP:;
			}
			else if(edit_mode==EDIT_METER)
			{
				if ((buttons[BT_RIGHT]&BT_LONG)==BT_SHORT)
				{
					if(++_item->params.meter>=METER_MAX)
						_item->params.meter=METER_EMPTY+1;
				}
				else if ((buttons[BT_LEFT]&BT_LONG)==BT_SHORT)
				{
					if((int8_t)--_item->params.meter<=METER_EMPTY)
						_item->params.meter=METER_MAX-1;
				}
				else if ((buttons[BT_CENTER]&BT_LONG)==BT_SHORT)
				{
					edit_mode=EDIT_ACC;
					_item->select=EDIT_TO_SELECT(EDIT_ACC);
				}
			}
			else if(edit_mode==EDIT_ACC)
			{
				if ((buttons[BT_RIGHT]&BT_LONG)==BT_SHORT)
				{
					if(++_item->params.accuracy>=ITEM_ACCURACY_MAX)
						_item->params.accuracy=ITEM_ACCURACY_0;
				}
				else if ((buttons[BT_LEFT]&BT_LONG)==BT_SHORT)
				{
					if(--(int8_t)_item->params.accuracy<0)
						_item->params.accuracy=ITEM_ACCURACY_MAX-1;
				}
				else if ((buttons[BT_CENTER]&BT_LONG)==BT_SHORT)
				{
					edit_mode=EDIT_SELECT;
					_item->select=EDIT_TO_SELECT(EDIT_SELECT);
				}
			}
			if ((buttons[BT_CENTER]&BT_LONG)==BT_LONG)
			{
				ScreenSave();
				edit_mode=EDIT_NONE;
				_item->select=EDIT_TO_SELECT(EDIT_NONE);
				idx=0;
				bt_release=1;
			}
		}
	}
	DrawScreen();
}

extern TaskHandle_t pxButtonsTaskHandle;
extern TaskHandle_t pxADCTaskHandle;
extern TaskHandle_t pxBCTaskHandle;
extern TaskHandle_t pxDisplayTaskHandle;
TaskHandle_t IdleTaskHandle;

TaskHandle_t PROGMEM *tasks[]={&pxDisplayTaskHandle,&pxButtonsTaskHandle,&pxADCTaskHandle,&pxBCTaskHandle,&IdleTaskHandle};
void DrawStats(void)
{
//	BaseType_t res=DispReadBT();
//	if ((res==pdPASS)&&(bt_release==0))
//	{
//		if ((buttons[BT_CENTER]&BT_LONG)==BT_SHORT)
//		{
//			DrawFunction=&DrawMenu;
//			return;
//		}
//	}
//	u8g2_SetFont(&u8g2, ITEM_SMALL_VAL);
//	u8g2_SetFontRefHeightText(&u8g2);
//	u8g2_SetFontPosTop(&u8g2);
//	for(uint8_t i=0;i<ARRAYLEN(tasks);i++)
//	{
//			TaskHandle_t hdl=(TaskHandle_t)*((portPOINTER_SIZE_TYPE*)pgm_read_word(&tasks[i]));
//			char *tsk=pcTaskGetTaskName(hdl);
//			uint16_t wtr=uxTaskGetStackHighWaterMark(hdl);
//
//	}


//	sprintf_P(str,PSTR("%s %u"),tsk,wtr);
//	u8g2_DrawUTF8(&u8g2,64,0,str);
//	wtr=xPortGetFreeHeapSize();
//	sprintf_P(str,PSTR("С %u"),wtr);
//	u8g2_DrawUTF8(&u8g2,64,12,str);
}

extern SemaphoreHandle_t xMeterSemaphoreHandle;

__attribute__ ((OS_main)) void vDisplayTask(void *param)
{
	portTickType xLastUpdate;
	ItemListInit();
	ScreenListInit();
	ScreenRestore();

	u8g2_Setup_ks0108_128x64_spi_1(&u8g2,U8G2_R0,u8x8_byte_empty,u8x8_avr_delay);
	u8g2_InitDisplay(&u8g2);
	u8g2_SetPowerSave(&u8g2, 0);
	u8g2_SetFontMode(&u8g2,1);
	u8g2_SetDrawColor(&u8g2,2);
	u8g2_SetFontRefHeightText(&u8g2);

	IdleTaskHandle =xTaskGetIdleTaskHandle();
	xLastUpdate = xTaskGetTickCount();
	for(;;)
	{
		while(xSemaphoreTake(xMeterSemaphoreHandle,1000)==pdFAIL);
		u8g2_FirstPage(&u8g2);
		do{
			DrawFunction();
		}while(u8g2_NextPage(&u8g2));
		if(xTaskGetTickCount()-xLastUpdate>=pdMS_TO_TICKS(10000))
		{
			xLastUpdate=xTaskGetTickCount();
			eeprom_write_float(&eestore.cfg.dist,meters[METER_ODO2]);
		}
		xSemaphoreGive(xMeterSemaphoreHandle);
		vTaskDelay(50);
	}
}
