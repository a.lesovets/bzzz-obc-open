/*
* Copyright (C) 2020 Akim Lesovets
* This file is part of bzzz-obc-open <https://gitlab.com/a.lesovets/bzzz-obc-open>.
*
* bzzz-obc-open is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* bzzz-obc-open is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with bzzz-obc-open.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * common_structs.h
 *
 *  Created on: 10 окт. 2020 г.
 *      Author: AKIM
 */

#ifndef OBC_PROJDEFS_H_
#define OBC_PROJDEFS_H_
#include <math.h>
#include "lstlib.h"
#ifndef __AVR__
#define PROGMEM
#define PGM_P	char*
#define strcpy_P strcpy
#define pgm_read_byte(x) (*x)
#define pgm_read_word(x) (*x)
#define sprintf_P	sprintf
#define strtok_P	strtok
#define memcpy_P memcpy
#endif

#define HALL1	(1<<PD2)
#define HALL2	(1<<PD3)

//#define MAGNET_N	(30)
//#define WHEEL_DIA	(0.165)
//#define WHEEL_LEN	(WHEEL_DIA*M_PI)
//#define DIST_DELTA	(WHEEL_LEN/MAGNET_N)

#define ADC_REF		(3300)//(3300)
#define ADC_RANGE	(1024)


#define SCREENS_NUM	(4)
#define ITEMS_NUM	(12)
#define NUM(x)		(x+0x30)

#define METER_FORMAT_ACC_POS	(2)

#define ITEM_SMALL_PREFIX	FixedMedium_6
#define ITEM_SMALL_VAL		unifont
#define ITEM_SMALL_UNIT		FixedMedium_6
#define ITEM_BIG_PREFIX		unifont
#define ITEM_BIG_FONT		JetSet_32
//#define ITEM_BIG_UNIT		u8g2_font_10x20_t_cyrillic
#define ITEM_BIG_UNIT		unifont

#define ITEM_SIZE(x)		(x&3)
#define ITEM_CHANGEBLE(x)	((x&1)<<4)
#define ITEM_ACCURACY(x)		((x&3)<<2)

#define ITEM_HJUST(x)		(x&3)
#define ITEM_VJUST(x)		((x&3)<<2)

#define ITEM_PROP(s,a,c)	(ITEM_SIZE(s)|ITEM_ACCURACY(a)|ITEM_CHANGEBLE(c))
#define ITEM_JUST(h,v)	(ITEM_HJUST(h)|ITEM_VJUST(v))

#define ITEM_CLEAR(item) (memset(item,0,sizeof(item_t)))
#define SCREEN_CLEAR(screen) (memset(screen,0,sizeof(screen_t)))

#define CFG_MAGIC	0x4d474643
#define SCRN_MAGIC	0x4e524353
#define SCRN_END	0x5f444e45

enum eMeter
{
	METER_EMPTY=0,
	METER_VOLTAGE,//
	METER_CURRENT,//
	METER_POWER,//
	METER_SPEED,//
	METER_CONSUMPTION,//
	METER_ECO,//
	METER_ODO1,//
	METER_ODO2,//
	METER_RANGE,
	METER_THROTTLE,
	METER_CURRENT1,//
	METER_CURRENT2,//
	METER_POWER1,//
	METER_POWER2,//
	METER_RPM1,//
	METER_RPM2,//
//	METER_TEMPERATURE,
	METER_MAX
};

enum eADCMeters
{
	ADC_METER_VOLTAGE=0,
	ADC_METER_CURRENT1,
	ADC_METER_CURRENT2,
	ADC_METER_CURRENT,
	ADC_METER_CONSUMPTION,
//	ADC_METER_POWER1,
//	ADC_METER_POWER2,
//	ADC_METER_POWER,
	ADC_METER_MAX
};

enum eBCmeters
{
	BC_METER_RPM1=0,
	BC_METER_RPM2,
	BC_METER_ODO1,
	BC_METER_ODO2,
	BC_METER_SPEED,
	BC_METER_ECO,
	BC_METER_RANGE,
	BC_METER_MAX
};

enum eItemHJustify
{
	ITEM_H_JUSTIFY_LEFT=0,
	ITEM_H_JUSTIFY_RIGHT,
	ITEM_H_JUSTIFY_CENTER,
	ITEM_H_JUSTIFY_MAX
};

enum eItemVJustify
{
	ITEM_V_JUSTIFY_TOP=0,
	ITEM_V_JUSTIFY_BOTTOM,
	ITEM_V_JUSTIFY_CENTER,
	ITEM_V_JUSTIFY_MAX,
};

enum eItemSize
{
	ITEM_SIZE_SMALL=0,
	ITEM_SIZE_BIG,
	ITEM_SIZE_MAX
};

enum eItemChangeable
{
	ITEM_CHANGEABLE_OFF=0,
	ITEM_CHANGEABLE_ON,
	ITEM_CHANGEABLE_MAX
};

enum eItemAccuracy
{
	ITEM_ACCURACY_0=0,
	ITEM_ACCURACY_1,
	ITEM_ACCURACY_2,
	ITEM_ACCURACY_MAX
};

enum eItemSelect
{
	ITEM_SELECT_NOSELECT=0,
	ITEM_SELECT_SELECT,
	ITEM_SELECT_HALF,
	ITEM_SELECT_FULL,
};

typedef struct sItemParams
{
	uint8_t x;
	uint8_t y;
	uint8_t meter;
	union
	{
		struct
		{
			uint8_t size:2;
			uint8_t accuracy:2;
			uint8_t changeable:1;
			uint8_t pad3:3;
		};
		uint8_t prop;
	};
	union
	{
		struct
		{
			uint8_t hjust:2;
			uint8_t vjust:2;
			uint8_t pad4:4;
		};
		uint8_t just;
	};
}item_params_t;

typedef struct sScreen
{
	NODE node;
	LIST items;
}screen_t;

typedef struct sItem
{
	NODE node;
	item_params_t params;
	uint8_t select;
}item_t;

typedef struct sConfig
{
	uint16_t vbatscale;
	uint16_t vbatresdivval;
	uint16_t vbatresdivmultiplier;
	uint16_t ampoffset1;
	uint16_t ampoffset2;
	uint16_t ampscale;
	uint16_t wheel;
	uint16_t magnets;
	uint16_t battery;
	float    dist;
}config_t;

typedef struct sEEStore
{
	uint32_t cfg_magic;
	config_t cfg;
	uint8_t scrn_data[];
}eestore_t;

typedef enum
{
	BT_LEFT,
	BT_CENTER,
	BT_RIGHT,
	BT_MAX
}buttons_t;

enum bt_value
{
	BT_REL=0,
	BT_SHORT=0x1,
	BT_LONG=0x3
};

#endif /* OBC_PROJDEFS_H_ */
