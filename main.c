/*
* Copyright (C) 2020 Akim Lesovets
* This file is part of bzzz-obc-open <https://gitlab.com/a.lesovets/bzzz-obc-open>.
*
* bzzz-obc-open is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* bzzz-obc-open is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with bzzz-obc-open.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * main.c
 *
 *  Created on: 8 сент. 2020 г.
 *      Author: AKIM
 */
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include "KS0108.h"
#include "u8g2.h"
#include "lstlib/lstLib.h"
#include "obc_projdefs.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "tasks/ADCTask/ADCTask.h"
#include "tasks/ButtonsTask/ButtonsTask.h"
#include "tasks/BCTask/BCTask.h"
#include "tasks/DisplayTask/DisplayTask.h"
#include "u8x8_avr.h"
#include "avr/eeprom.h"


SemaphoreHandle_t	xMeterSemaphoreHandle=NULL;
QueueHandle_t		xBTQueueHandle=NULL;

TaskHandle_t	pxDisplayTaskHandle=NULL;
TaskHandle_t	pxButtonsTaskHandle=NULL;
TaskHandle_t	pxADCTaskHandle=NULL;
TaskHandle_t	pxBCTaskHandle=NULL;

config_t config;

config_t PROGMEM def_config={
		.vbatscale=310,
		.vbatresdivval=10,
		.vbatresdivmultiplier=1,
		.ampoffset1=0xffff,
		.ampoffset2=0xffff,
		.ampscale=403,
		.wheel=165,
		.magnets=30,
		.battery=20000,
		.dist=0.0f,
};

float meters[METER_MAX]={0,};
eestore_t EEMEM eestore;

void blinky(uint8_t n) __attribute__((naked));
void blinky(uint8_t n)
{
	asm("cli");
	DDRB=1<<PB5;
	SPCR=0;
	for(;;)
	{
		for(int i=0;i<n;i++)
		{
			PORTB|=1<<PB5;
			_delay_ms(100);
			PORTB&=~(1<<PB5);
			_delay_ms(300);
		}
		_delay_ms(1000);
	}
}

ISR(BADISR_vect)
{
	blinky(4);
}

void cfgassert(int a)
{
	if (a!=0)
		return;
	blinky(1);
}

void vApplicationStackOverflowHook( xTaskHandle *pxTsk, signed portCHAR *pcTskNm )
{
	blinky(3);
}

__attribute__ ((OS_main)) int main(void)
{
	DDRC=0;
	PORTC=0;
	DDRD=(1<<PD4);
	DDRB=0;
	PORTB=0;

	uint32_t cfg_magic;
	eeprom_read_block(&cfg_magic,&eestore.cfg_magic,sizeof(eestore.cfg_magic));
	if(cfg_magic==CFG_MAGIC)
		eeprom_read_block(&config,&eestore.cfg,sizeof(eestore.cfg));
	else
		ConfigReset();

	xMeterSemaphoreHandle=xSemaphoreCreateBinary();
	xSemaphoreGive(xMeterSemaphoreHandle);
	xBTQueueHandle=xQueueCreate(1,sizeof(enum bt_value[BT_MAX]));


	xTaskCreate(&vADCTask,"А",configMINIMAL_STACK_SIZE*2,NULL,4,&pxADCTaskHandle);
	xTaskCreate(&vButtonsTask,"К",configMINIMAL_STACK_SIZE*1,NULL,2,&pxButtonsTaskHandle);
	xTaskCreate(&vDisplayTask,"Д",configMINIMAL_STACK_SIZE*4,NULL,1,&pxDisplayTaskHandle);
	xTaskCreate(&vBCTask,"Б",configMINIMAL_STACK_SIZE*2,NULL,3,&pxBCTaskHandle);

//	if (res<4)
//		blinky(2);

	vTaskStartScheduler();
	for(;;);
	return 0;
}
