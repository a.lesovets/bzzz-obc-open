/*
* Copyright (C) 2020 Akim Lesovets
* This file is part of bzzz-obc-open <https://gitlab.com/a.lesovets/bzzz-obc-open>.
*
* bzzz-obc-open is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* bzzz-obc-open is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with bzzz-obc-open.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "stdint.h"
#include "u8g2.h"

#define LCD_E	(0)
#define LCD_CD	(1)
#define LCD_CS1	(3)
#define LCD_CS2	(2)
#define LCD_RES	(7)

#define LATCH			(PB2)
#define LATCH_PORT		(PORTB)
#define LATCH_DDR		(DDRB)

uint8_t u8x8_cad_hc595(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr);
void u8g2_Setup_ks0108_128x64_spi_1(u8g2_t *u8g2, const u8g2_cb_t *rotation, u8x8_msg_cb byte_cb, u8x8_msg_cb gpio_and_delay_cb);
void u8g2_Setup_ks0108_128x64_spi_2(u8g2_t *u8g2, const u8g2_cb_t *rotation, u8x8_msg_cb byte_cb, u8x8_msg_cb gpio_and_delay_cb);
void u8g2_Setup_ks0108_128x64_spi_f(u8g2_t *u8g2, const u8g2_cb_t *rotation, u8x8_msg_cb byte_cb, u8x8_msg_cb gpio_and_delay_cb);

