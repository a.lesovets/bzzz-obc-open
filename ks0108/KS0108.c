/*
* Copyright (C) 2020 Akim Lesovets
* This file is part of bzzz-obc-open <https://gitlab.com/a.lesovets/bzzz-obc-open>.
*
* bzzz-obc-open is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* bzzz-obc-open is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with bzzz-obc-open.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KS0108.h"
#include <avr/io.h>
#include <util/delay.h>
#include "u8g2.h"
#include "FreeRTOS.h"

static uint8_t pin_state=0;

static void spi_send(uint8_t data)
{
	SPDR=data;
	while(!(SPSR & (1<<SPIF)));
}

static void hc595_latch(void)
{
	LATCH_PORT|=(1<<LATCH);
	__asm__("nop");
	LATCH_PORT&=~(1<<LATCH);
}

static void hc595_Init(void)
{
	DDRB|=(1<<PB3)|(1<<PB5);
	LATCH_DDR|=(1<<LATCH);
	SPCR=(0<<CPOL)|(0<<SPR0)|(0<<DORD)|(1<<MSTR)|(1<<SPE);
	SPSR|=(1<SPI2X);
}

static void hc595_send(uint8_t data1,uint8_t data2)
{
	portENTER_CRITICAL();
	spi_send(data1);
	spi_send(data2);
	hc595_latch();
	portEXIT_CRITICAL();
}

static void KS0108_Send(uint8_t data)
{
	pin_state|=(1<<LCD_E);
	hc595_send(pin_state,data);
	pin_state&=~(1<<LCD_E);
	hc595_send(pin_state,data);
}

static void KS0108_Reset(uint8_t state)
{
	pin_state=(state<<LCD_RES);
	hc595_send(pin_state,0);
}

static void KS0108_SetEnable(uint8_t data)
{
	pin_state=(data&1)?(pin_state|(1<<LCD_CS1)):(pin_state&~(1<<LCD_CS1));
	pin_state=(data&2)?(pin_state|(1<<LCD_CS2)):(pin_state&~(1<<LCD_CS2));
	hc595_send(pin_state,0);
}

static void KS0108_WriteCommand(uint8_t data)
{
	pin_state&=~(1<<LCD_CD);
	KS0108_Send(data);
}

static void KS0108_WriteData(uint8_t data)
{
	pin_state|=(1<<LCD_CD);
	KS0108_Send(data);
}

uint8_t u8x8_cad_hc595(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr)
{
	uint8_t *data;
	switch(msg)
	{
	case U8X8_MSG_CAD_SEND_CMD:
	case U8X8_MSG_CAD_SEND_ARG:
		KS0108_WriteCommand(arg_int);
		break;
	case U8X8_MSG_CAD_SEND_DATA:
		data = (uint8_t *)arg_ptr;
		while( arg_int-- )
			KS0108_WriteData(*data++);
		break;
	case U8X8_MSG_CAD_INIT:
		hc595_Init();
		KS0108_Reset(0);
		_delay_us(100);
		KS0108_Reset(1);
		break;
	case U8X8_MSG_CAD_START_TRANSFER:
	case U8X8_MSG_CAD_END_TRANSFER:
		KS0108_SetEnable(arg_int);
		break;
	default:
		return 0;
	}
	return 1;
}

void u8g2_Setup_ks0108_128x64_spi_1(u8g2_t *u8g2, const u8g2_cb_t *rotation, u8x8_msg_cb byte_cb, u8x8_msg_cb gpio_and_delay_cb)
{
	uint8_t tile_buf_height;
	uint8_t *buf;
	u8g2_SetupDisplay(u8g2, u8x8_d_ks0108_128x64, u8x8_cad_hc595, byte_cb, gpio_and_delay_cb);
	buf = u8g2_m_16_8_1(&tile_buf_height);
	u8g2_SetupBuffer(u8g2, buf, tile_buf_height, u8g2_ll_hvline_vertical_top_lsb, rotation);
}
/* ks0108 2 */
void u8g2_Setup_ks0108_128x64_spi_2(u8g2_t *u8g2, const u8g2_cb_t *rotation, u8x8_msg_cb byte_cb, u8x8_msg_cb gpio_and_delay_cb)
{
	uint8_t tile_buf_height;
	uint8_t *buf;
	u8g2_SetupDisplay(u8g2, u8x8_d_ks0108_128x64, u8x8_cad_hc595, byte_cb, gpio_and_delay_cb);
	buf = u8g2_m_16_8_2(&tile_buf_height);
	u8g2_SetupBuffer(u8g2, buf, tile_buf_height, u8g2_ll_hvline_vertical_top_lsb, rotation);
}
/* ks0108 f */
void u8g2_Setup_ks0108_128x64_spi_f(u8g2_t *u8g2, const u8g2_cb_t *rotation, u8x8_msg_cb byte_cb, u8x8_msg_cb gpio_and_delay_cb)
{
	uint8_t tile_buf_height;
	uint8_t *buf;
	u8g2_SetupDisplay(u8g2, u8x8_d_ks0108_128x64, u8x8_cad_hc595, byte_cb, gpio_and_delay_cb);
	buf = u8g2_m_16_8_f(&tile_buf_height);
	u8g2_SetupBuffer(u8g2, buf, tile_buf_height, u8g2_ll_hvline_vertical_top_lsb, rotation);
}
